<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string $email
 * @property string $username
 * @property string $authKey
 * @property int|null $activo
 * @property string|null $password
 * @property string $accessToken
 * @property int $role
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'username', 'authKey', 'accessToken'], 'required'],
            [['activo', 'role'], 'integer'],
            [['nombre', 'apellidos', 'authKey', 'password', 'accessToken'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 50],
            [['username'], 'string', 'max' => 55],
            [['email'], 'unique'],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
            'username' => 'Username',
            'authKey' => 'Auth Key',
            'activo' => 'Activo',
            'password' => 'Password',
            'accessToken' => 'Access Token',
            'role' => 'Role',
        ];
    }
    
     public function beforeSave($insert) {
         if (parent::beforeSave($insert)) {

                $this->authKey = \Yii::$app->security->generateRandomString();
                $this->password = Yii::$app->security->generatePasswordHash($this->password);
                return true;
         }
    }
    
}
