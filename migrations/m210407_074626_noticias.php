<?php

use yii\db\Schema;
use yii\db\Migration;

class m210407_074626_noticias extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%noticias}}',
            [
                'id'=> $this->primaryKey(11),
                'titulo'=> $this->string(255)->null()->defaultValue(null),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%noticias}}');
    }
}
