<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%poblaciones}}`.
 */
class m210406_094830_create_poblaciones_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%poblaciones}}', [
            'idPoblacion' => $this->primaryKey(),
            'nombre' => $this->string(55)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%poblaciones}}');
    }
}
